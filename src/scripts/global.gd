#Copyright (C) 2020  Helena Aires

extends Node

var colors = {
	Enums.Scenes.PASTEL: Color("b08363"),
	Enums.Scenes.BLUE: Color("8998c2"),
	Enums.Scenes.PINK: Color("ba5b65")
	} setget, get_current_color

var level = Enums.Levels.CHILDHOOD setget set_level, get_level
var scene = Enums.Scenes.PASTEL setget set_scene, get_scene

# Total count of memories on current level
var memory_count = 0 setget set_memory_count, get_memory_count
# Count of recovered memories on current level
var current_memories = 0 setget set_current_memories, get_current_memories
# Count of recovered memories throughout the game
var total_memories = 0 setget set_total_memories, get_total_memories

func set_level(new_level):
	level = new_level

func get_level():
	return level

func got_to_next_level():
	level += 1

func set_scene(new_scene):
	scene = new_scene

func get_scene():
	return scene

func get_current_color():
	return colors[scene]

func get_color(color):
	return colors[color]

func set_memory_count(count):
	memory_count = count

func get_memory_count():
	return memory_count

func set_current_memories(count):
	current_memories = count

func get_current_memories():
	return current_memories

func add_memory():
	current_memories += 1
	total_memories += 1
	if current_memories == memory_count:
		return true
	return false

func set_total_memories(count):
	total_memories = count

func get_total_memories():
	return total_memories

func save():
	pass
