#Copyright (C) 2020  Helena Aires

extends Popup

signal pathway_selected

var current_color = Color()

func about_to_show():
	current_color = Global.get_current_color()
	
	$VBoxContainer/Label.set("custom_colors/font_color",current_color)
	_change_button_color($VBoxContainer/pastel)
	_change_button_color($VBoxContainer/blue)
	_change_button_color($VBoxContainer/pink)

func _change_button_color(button):
	button.set("custom_colors/font_color_disabled",current_color)
	button.set("custom_colors/font_color",current_color)
	button.set("custom_colors/font_color_hover",current_color)
	button.set("custom_colors/font_color_pressed",current_color)

func _pastel_click():
	emit_signal("pathway_selected", Enums.Scenes.PASTEL)
	hide()

func _blue_click():
	emit_signal("pathway_selected", Enums.Scenes.BLUE)
	hide()

func _pink_click():
	emit_signal("pathway_selected", Enums.Scenes.PINK)
	hide()
