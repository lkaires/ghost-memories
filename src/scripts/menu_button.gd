#Copyright (C) 2020  Helena Aires

extends Control

signal menu_clicked

export(preload("res://scripts/enums.gd").Menu) var option = Enums.Menu.PAUSE

var expanded = false

func _ready():
	var text = ""
	match option:
		Enums.Menu.PAUSE:
			text = "Pause"
		Enums.Menu.LOG:
			text = "Log"
		Enums.Menu.CONFIG:
			text = "Menu"
		Enums.Menu.QUIT:
			text = "Quit"
	$Sprite/Label.set_text(text)
	
	connect("menu_clicked",get_tree().current_scene,"on_menu_clicked")

func update():
	var color = Global.get_current_color()
#	$Sprite/Label.set("custom_colors/font_color", color)
	$ColorRect.set_frame_color(color)
	$Sprite.set_frame(Global.get_scene())


func _on_mouse_entered():
	if !expanded:
		$AnimationPlayer.play("mouse_on")

func _on_mouse_exited():
	if expanded:
		$AnimationPlayer.play("mouse_off")

func _on_expand_finished():
	if expanded:
		expanded = false
	else:
		expanded = true

func _on_click():
	emit_signal("menu_clicked",option)
