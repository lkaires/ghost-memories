#Copyright (C) 2020  Helena Aires

extends StaticBody2D

signal opened_chest

var open = false setget set_open
var _frame_coords = Vector2.ZERO

onready var sprite = $Sprite

func _ready():
	connect("opened_chest",get_tree().current_scene,"open_chest")

func update():	
	match Global.get_scene():
		Enums.Scenes.PASTEL:
			_frame_coords.x = 0
		Enums.Scenes.BLUE:
			_frame_coords.x = 1
		Enums.Scenes.PINK:
			_frame_coords.x = 2
	
	if open:
		_frame_coords.y = 1
	sprite.set_frame_coords(_frame_coords)

func set_open(new_open):
	if new_open == open:
		return
	
	if new_open:
		_frame_coords.y = 1
	else:
		_frame_coords.y = 0
	sprite.set_frame_coords(_frame_coords)

func _on_interaction(area):
	if !open:
		set_open(true)
		emit_signal("opened_chest",get_position_in_parent())
